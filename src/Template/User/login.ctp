
<div class="d-flex justify-content-center">
  <h1> Login</h1>
  <form>

    <div class="form-group">
      <label for="email">Email address</label>
      <input type="email" class="form-control" id="email" placeholder="Enter your email">

    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" placeholder="Enter your password">
    </div>

    <button type="submit" class="btn btn-primary">Login</button>
  </form>
</div>
