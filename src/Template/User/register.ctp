<div class="d-flex justify-content-center">
  <h1> Register account </h1>
<!--
  <form>
      <div class="form-group">
        <label for="fullName">Full name</label>
        <input type="email" class="form-control" id="fullName" placeholder="Enter you name">

      </div>
    <div class="form-group">
      <label for="email">Email address</label>
      <input type="email" class="form-control" id="email" placeholder="Enter email">

    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" placeholder="Choose a password">
    </div>
    <div class="form-group">
      <label for="retypePassword">Retype password</label>
      <input type="password" class="form-control" id="retypePassword" placeholder="Please retype your password">
    </div>
    <button type="submit" class="btn btn-primary">Register</button>
  </form>
</div>
-->

<?= $this->Form->create(); ?>
	<?= $this->Form->input('fullName', array(
								'label' => 'Full name',
								'class' => 'form-control'
							)); ?>
	<?= $this->Form->input('email', array(
								'label' => 'Email',
								'class' => 'form-control'
							)); ?>
	<?= $this->Form->input('password', array(
								'label' => 'Choose a password',
								'class' => 'form-control'
							)); ?>
	<?= $this->Form->input('repeatPassword', array(
								'label' => 'Please repeat your password',
								'class' => 'form-control'
							)); ?>
	<hr>

	<?= $this->Form->submit('Submit', array('class' => 'btn btn-primary')); ?>
<?= $this->Form->end(); ?>
