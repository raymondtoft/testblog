<?php
    namespace App\Controller;

    use App\Controller\AppController;

    class PostController extends AppController
    {
      public function index(){
        //$this->Post->recursive = 0;
        $this->set('posts', $this->Paginator->paginate());
      }
      public function view ( $id = null){
        if(!$this->Post->exists($id)) {
          throw NotFoundException(__('INvalid post'));
        }
        $this->set('post', $this->Post->find($id));
      }
    }
