<?php
    namespace App\Controller;

    use App\Controller\AppController;

    class LoginController extends AppController
    {
      /**
       * Index method
       *
       * @return void
       */
      public function index()
      {
          $this->set('login', $this->paginate($this->Users));
          $this->set('_serialize', ['login']);
      }
    }
